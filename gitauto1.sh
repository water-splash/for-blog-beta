#! /bin/bash
# program
#   for automatically git commit and push
#2021/05/27 kawhicurry
#beta 1.0

#switch branch first(create one if 'dev' branch do not exist)
git checkout dev 2> ./tmp
if test ./tmp -e ;then
read -rp "dev branch doesn't exist now,do you want to new one?(type y or yes)" yes
if [ "${yes}" == "y" ] || [ "${yes}" == "yes" ];then
    git branch dev
    echo "build 'dev' success!"
else
    exit 1
fi
fi

#commit then
git add .
git status
case $# in
"0")
read -rp "Please input commit message:" message
git commit -m "${message}"
;;
"1")
git commit -m "$1"
echo "commit success"
;;
*)
echo "too many arguments!,please retry"
git checkout main
exit 2
;;
esac

git pull
git checkout main
git merge dev
git push
echo "sync success!"

exit 0
